package org.mat3.test.repository;

import org.mat3.test.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> getAllByUidEqualsAndTimeGreaterThan(String uid, LocalDateTime time);
    boolean existsOrdersByTidEquals(String tid);
}
