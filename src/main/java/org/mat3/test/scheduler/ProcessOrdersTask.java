package org.mat3.test.scheduler;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mat3.test.model.Order;
import org.mat3.test.model.Status;
import org.mat3.test.service.OrderService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@EnableAsync
@Component
@AllArgsConstructor
public class ProcessOrdersTask {
    OrderService orderService;

    @Async
    public void checkOrders() {
        List<Order> orders = orderService.findAll();
        for (Order order : orders) {
            if (order.getStatus().equals(Status.ACCEPTED) || order.getStatus().equals(Status.PROCESSING)) {
                RestTemplate restTemplate = new RestTemplate();
                JsonNode result = restTemplate.getForObject("http://localhost:8080/api/payments/process", JsonNode.class);
                order.setStatus(Status.valueOf(result.get("status").asText()));
                orderService.save(order);
                log.info("Update order #" + order.getId() + " set payment status " + order.getStatus().name());
            }
        }
    }
}