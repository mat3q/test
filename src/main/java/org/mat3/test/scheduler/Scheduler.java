package org.mat3.test.scheduler;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class Scheduler {
    private ProcessOrdersTask async;

    @Scheduled(fixedRate = 10000)
    public void timer() {
       async.checkOrders();
    }
}