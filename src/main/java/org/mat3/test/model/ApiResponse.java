package org.mat3.test.model;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;


@Slf4j
public class ApiResponse {
    private Map<String, Object> body = new HashMap<>();
    private String msg = "";

    ApiResponse() {}

    public static ResponseEntity error(String message, String path, HttpStatus status) {
        Map<String, Object> body = new HashMap<>();
        body.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        body.put("status", status.value());
        body.put("error", status.getReasonPhrase());
        body.put("message", message);
        body.put("path", path);
        log.error(message);
        return new ResponseEntity(body, status);
    }

    public static ApiResponse builder() { return new ApiResponse(); }

    public ApiResponse add(String name, Object value) {
        body.put(name, value);
        return this;
    }

    public ApiResponse log(String msg) {
        this.msg = msg;
        return this;
    }

    public ResponseEntity build(HttpStatus status, Object body) {
        if (!msg.isEmpty())
            log.info(msg);
        return new ResponseEntity(body, status);
    }
    public ResponseEntity build(HttpStatus status) { return build(status, body); }
}