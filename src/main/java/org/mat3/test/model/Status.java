package org.mat3.test.model;

public enum Status {
    ACCEPTED, PROCESSING, ERROR, COMPLETED;
}