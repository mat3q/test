package org.mat3.test.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity @Table(name = "orders")
@Data
public class Order {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String uid = UUID.randomUUID().toString();
    private String tid;
    private int route_number;
    private LocalDateTime time;

    @Enumerated(EnumType.STRING)
    private Status status = Status.ACCEPTED;


}

