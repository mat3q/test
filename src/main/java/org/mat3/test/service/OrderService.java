package org.mat3.test.service;

import lombok.RequiredArgsConstructor;
import org.mat3.test.model.Order;
import org.mat3.test.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository repository;

    public List<Order> findAll() {
        return repository.findAll();
    }
    public Optional<Order> findById(int id) {
        return repository.findById(id);
    }
    public List<Order> findByUID(String uid) {return repository.getAllByUidEqualsAndTimeGreaterThan(uid, LocalDateTime.now());}
    public Order save(Order order) {
        return repository.save(order);
    }
    public boolean isOrdersExists(String tid) {return repository.existsOrdersByTidEquals(tid);}
}
