package org.mat3.test.api;

import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mat3.test.model.ApiResponse;
import org.mat3.test.model.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@Slf4j
@Controller
@Api(tags = "Payment", description = "APIs for process payments")
@RequestMapping("/api/payments/")
@RequiredArgsConstructor
public class PaymentApi {

    @ApiOperation("Process payment")
    @ApiResponses(value = @io.swagger.annotations.ApiResponse(
            code = 200,
            message = "Return random payment status",
            examples = @Example(value = {@ExampleProperty(value = "\t\n" +
                    "{\"status\": \"RANDOM_STATUS\"}", mediaType = "application/json")})
    ))
    @RequestMapping(method = RequestMethod.GET, path = "/process", produces = "application/json")
    @ResponseBody
    ResponseEntity status() {
        Status random = Status.values()[new Random().nextInt(3)+1];
        return ApiResponse.builder().
                add("status", random.name()).
                build(HttpStatus.OK);
    }
}
