package org.mat3.test.api;

import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.mat3.test.model.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

@Slf4j
@Controller
@Api(tags = "User", description = "APIs for manage users")
@RequestMapping("/api/users/")
public class UserApi {
    @ApiOperation("Generate new UID")
    @ApiResponses(value = @io.swagger.annotations.ApiResponse(
            code = 200,
            message = "Create new user identification number",
            examples = @Example(value = {@ExampleProperty(value = "\t\n" +
                    "{\"uid\": \"c9f768be-e364-4dd2-aa19-fd4868231dc7\"}", mediaType = "application/json")})
    ))
    @RequestMapping(method = RequestMethod.GET, path = "/token", produces = "application/json")
    @ResponseBody
    ResponseEntity getUserID() {
        String uid = UUID.randomUUID().toString();
        return ApiResponse.builder().
                add("uid", uid).
                log("Created new UID #" + uid).
                build(HttpStatus.CREATED);
    }
}
