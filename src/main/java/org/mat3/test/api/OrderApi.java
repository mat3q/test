package org.mat3.test.api;

import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mat3.test.model.ApiResponse;
import org.mat3.test.model.Order;
import org.mat3.test.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Controller
@Api(tags = "Order", description = "APIs for working with orders")
@RequestMapping("/api/orders/")
@RequiredArgsConstructor
public class OrderApi {
    private final OrderService service;

    @ApiOperation("Create new order")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(
                code = 200,
                message = "Create and return order id",
                examples = @Example(value = {@ExampleProperty(value = "{\"id\":0}", mediaType = "application/json")})
            ),
            @io.swagger.annotations.ApiResponse(
                code = 422,
                message = "Generate error when order already exists"
            )
    })
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    ResponseEntity create(@RequestBody Order obj) {
        UUID ticketID = UUID.nameUUIDFromBytes((obj.getUid() + obj.getRoute_number() + obj.getTime().format(DateTimeFormatter.ISO_DATE_TIME)).getBytes());
        obj.setTid(ticketID.toString());

        if (!service.isOrdersExists(ticketID.toString())) {
            service.save(obj);
            return ApiResponse.builder().
                    add("id", obj.getId())
                    .log("Order " + obj.getId() + " created successfully.")
                    .build(HttpStatus.OK);
        } else
            return ApiResponse.error(
                "Error when created order with TicketID #" + ticketID + ". Already exists!",
                "/api/orders/",
                HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ApiOperation("Find order by id or uid")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(
                    code = 200,
                    message = "Return status of order",
                    examples = @Example(value = {@ExampleProperty(value = "{\"status\": \"COMPLETED\"}", mediaType = "application/json")})
            ),
            @io.swagger.annotations.ApiResponse(
                    code = 404,
                    message = "Generate error when order with this id not found"
            )
    })
    @RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
    @ResponseBody
    ResponseEntity get(@PathVariable String id, @RequestParam(required = false, defaultValue = "false") boolean findByUID) {
        if (findByUID) {
            List<Order> orders = service.findByUID(id);

            if (CollectionUtils.isEmpty(orders)) {
                return ApiResponse.error(
                        "Orders created by UID #" + id + " not found!",
                        "/api/orders/" + id,
                        HttpStatus.NOT_FOUND);
            } else {
                orders.sort(Comparator.comparing(Order::getTime));
                return ApiResponse.builder().
                        log("Order created by UID:" + id + " and datetime > " + LocalDateTime.now() + " successfully parsed and sorted. Count: " + orders.size()).
                        build(HttpStatus.OK, orders);

            }
        } else {
            Optional<Order> order = service.findById(Integer.parseInt(id));
            if (!order.isPresent())
                return ApiResponse.error(
                        "Order with ID #" + id + " not found!",
                        "/api/orders/" + id,
                        HttpStatus.NOT_FOUND);
            else
                return ApiResponse.
                        builder().
                        add("status", order.get().getStatus().name()).
                        log("Order with ID #" + id + "founded! Status: " + order.get().getStatus()).
                        build(HttpStatus.OK);

        }
    }
}
